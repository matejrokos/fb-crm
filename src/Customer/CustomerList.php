<?php declare(strict_types = 1);

namespace App\Customer;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class CustomerList extends EntityList
{

    /** @param \App\Customer\Customer[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, Customer::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\Customer\Customer[]
     */
    public function getItems(): array
    {
        /** @var \App\Customer\Customer[] $customers */
        $customers = parent::getItems();
        return $customers;
    }
}

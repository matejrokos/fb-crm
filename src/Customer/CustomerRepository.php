<?php declare(strict_types = 1);

namespace App\Customer;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class CustomerRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getCustomerById(UuidInterface $id): Customer
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select([
                'customer',
            ])
            ->from(Customer::class, 'customer')
            ->andWhere('customer.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllCustomers(): CustomerList
    {
        $customers = $this->entityManager
            ->createQueryBuilder()
            ->select([
                'customer',
            ])
            ->from(Customer::class, 'customer')
            ->orderBy('customer.name')
            ->getQuery()
            ->getResult();

        return new CustomerList($customers);
    }

    public function saveCustomer(Customer $customer): void
    {
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }

    public function deleteCustomer(Customer $customer): void
    {
        $this->entityManager->remove($customer);
        $this->entityManager->flush();
    }
}

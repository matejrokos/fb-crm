<?php declare(strict_types = 1);

namespace App\Customer;

use App\ContactPerson\ContactPerson;
use App\User\User;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class CustomerFactory
{

    public function createCustomer(
        ?string $identificationNumber,
        string $name,
        ?string $address,
        ?string $deliveryAddress,
        ?ContactPerson $contactPersonPrimary,
        ?ContactPerson $contactPersonSecondary,
        ?CustomerType $customerType,
        ?string $note,
        User $user
    ): Customer
    {
        return new Customer(
            Uuid::uuid4(),
            $name,
            $identificationNumber,
            $address,
            $deliveryAddress,
            $contactPersonPrimary,
            $contactPersonSecondary,
            $customerType,
            $note,
            $user,
            new DateTimeImmutable()
        );
    }
}

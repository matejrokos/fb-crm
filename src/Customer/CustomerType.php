<?php declare(strict_types = 1);

namespace App\Customer;

use Consistence\Enum\Enum;

class CustomerType extends Enum
{

    public const BUSINESS = 'business';
    public const PRIVATE_PERSON = 'private-person';
}

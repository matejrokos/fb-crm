<?php declare(strict_types = 1);

namespace App\Customer;

use App\ContactPerson\ContactPerson;
use App\User\User;
use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class Customer
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @var string|null
     */
    private $identificationNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $deliveryAddress;

    /**
     * @ORM\Embedded(class=ContactPerson::class)
     * @var \App\ContactPerson\ContactPerson|null
     */
    private $contactPersonPrimary;

    /**
     * @ORM\Embedded(class=ContactPerson::class)
     * @var \App\ContactPerson\ContactPerson|null
     */
    private $contactPersonSecondary;

    /**
     * @Enum(class=CustomerType::class)
     * @ORM\Column(type="string_enum", nullable=true)
     * @var \App\Customer\CustomerType|null
     */
    private $customerType;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="\App\User\User")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\User\User
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        string $name,
        ?string $identificationNumber,
        ?string $address,
        ?string $deliveryAddress,
        ?ContactPerson $contactPersonPrimary,
        ?ContactPerson $contactPersonSecondary,
        ?CustomerType $customerType,
        ?string $note,
        User $user,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->identificationNumber = $identificationNumber;
        $this->address = $address;
        $this->deliveryAddress = $deliveryAddress;
        $this->contactPersonPrimary = $contactPersonPrimary;
        $this->contactPersonSecondary = $contactPersonSecondary;
        $this->customerType = $customerType;
        $this->note = $note;
        $this->user = $user;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    public function setIdentificationNumber(?string $identificationNumber): void
    {
        $this->identificationNumber = $identificationNumber;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getDeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?string $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getContactPersonPrimary(): ?ContactPerson
    {
        return $this->contactPersonPrimary;
    }

    public function setContactPersonPrimary(?ContactPerson $contactPersonPrimary): void
    {
        $this->contactPersonPrimary = $contactPersonPrimary;
    }

    public function setContactPersonSecondary(?ContactPerson $contactPersonSecondary): void
    {
        $this->contactPersonSecondary = $contactPersonSecondary;
    }

    public function getContactPersonSecondary(): ?ContactPerson
    {
        return $this->contactPersonSecondary;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    public function getCustomerType(): ?CustomerType
    {
        return $this->customerType;
    }

    public function setCustomerType(?CustomerType $customerType): void
    {
        $this->customerType = $customerType;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }
}

<?php declare(strict_types = 1);

namespace App\ContactPerson;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class ContactPerson
{

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=100, options={"collation": "ascii_general_ci"})
     * @var string|null
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true, length=30)
     * @var string|null
     */
    private $phone;

    public function __construct(
        ?string $name,
        ?string $email,
        ?string $phone
    )
    {
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function hasName(): bool
    {
        return $this->name !== null;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function hasEmail(): bool
    {
        return $this->email !== null;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function hasPhone(): bool
    {
        return $this->phone !== null;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }
}

<?php declare(strict_types = 1);

namespace App\ContactPerson;

use App\AdminApp\ContactPerson\ContactPersonRequest;

class ContactPersonRequestProcessor
{

    public function createFromContactPersonRequest(?ContactPersonRequest $contactPersonRequest): ?ContactPerson
    {
        if ($contactPersonRequest === null) {
            return null;
        }

        return new ContactPerson(
            $contactPersonRequest->name,
            $contactPersonRequest->email,
            $contactPersonRequest->phone
        );
    }

    public function editContactPerson(
        ContactPerson $contactPerson,
        ContactPersonRequest $contactPersonRequest
    ): ContactPerson
    {
        $contactPerson->setName($contactPersonRequest->name);
        $contactPerson->setEmail($contactPersonRequest->email);
        $contactPerson->setPhone($contactPersonRequest->phone);
        return $contactPerson;
    }
}

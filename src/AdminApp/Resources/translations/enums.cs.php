<?php declare(strict_types = 1);

use App\Insurance\PaymentFrequency;
use App\User\UserRole;

return [
    PaymentFrequency::class . ':' . PaymentFrequency::MONTHLY => 'Měsíčně',
    PaymentFrequency::class . ':' . PaymentFrequency::HALF_YEARLY => 'Pololetně',
    PaymentFrequency::class . ':' . PaymentFrequency::QUARTERLY => 'Kvartálně',
    PaymentFrequency::class . ':' . PaymentFrequency::YEARLY => 'Ročně',
    UserRole::class . ':' . UserRole::ADMIN => 'Správce',
    UserRole::class . ':' . UserRole::EMPLOYEE => 'Zaměstnanec',
];

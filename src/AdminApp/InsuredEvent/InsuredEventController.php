<?php declare(strict_types = 1);

namespace App\AdminApp\InsuredEvent;

use App\Insurance\ContractRepository;
use App\InsuredEvent\InsuredEventFactory;
use App\InsuredEvent\InsuredEventRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/insured-event")
 */
class InsuredEventController extends AbstractController
{

    /** @var \App\InsuredEvent\InsuredEventRepository */
    private $insuredEventRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    /** @var \App\InsuredEvent\InsuredEventFactory */
    private $insuredEventFactory;

    public function __construct(
        InsuredEventRepository $insuredEventRepository,
        FormFactoryInterface $formFactory,
        InsuredEventFactory $insuredEventFactory
    )
    {
        $this->insuredEventRepository = $insuredEventRepository;
        $this->formFactory = $formFactory;
        $this->insuredEventFactory = $insuredEventFactory;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.insuredEvent.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/InsuredEvent/list.html.twig', [
            'insuredEvents' => $this->insuredEventRepository->findAllInsuredEvents(),
        ]);
    }

    /**
     * @Route("/new/{contractId}", name="adminApp.insuredEvent.create")
     */
    public function createAction(
        string $contractId,
        Request $request,
        ContractRepository $contractRepository
    ): Response
    {
        $contract = $contractRepository->getContractById(Uuid::fromString($contractId));
        $insuredEventRequest = new InsuredEventRequest();
        $insuredEventRequest->contract = $contract;
        $form = $this->formFactory->create(
            InuredEventFormType::class,
            $insuredEventRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $insuredEvent = $this->insuredEventFactory->createInsuredEvent(
                $insuredEventRequest,
                $contract
            );
            $this->insuredEventRepository->saveInsuredEvent($insuredEvent);
            $this->addFlash(
                'success',
                'Nová pojistná událost je vytvořena.'
            );
            return $this->redirectToRoute('adminApp.insuredEvent.list');
        }
        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nová pojistná událost',
            'goBackLink' => $this->generateUrl('adminApp.insuredEvent.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/", name="adminApp.insuredEvent.detail")
     */
    public function detailAction(string $id): Response
    {
        $insuredEvent = $this->insuredEventRepository->getInsuredEventById(
            Uuid::fromString($id)
        );

        return $this->render('@AdminApp/InsuredEvent/detail.html.twig', [
            'insuredEvent' => $insuredEvent,
            'goBackLink' => $this->generateUrl('adminApp.insuredEvent.list'),
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="adminApp.insuredEvent.edit")
     */
    public function editAction(
        string $id,
        Request $request
    ): Response
    {
        $insuredEvent = $this->insuredEventRepository->getInsuredEventById(
            Uuid::fromString($id)
        );

        $insuredEventRequest = InsuredEventRequest::fromInsuredEvent($insuredEvent);
        $form = $this->formFactory->create(
            InuredEventFormType::class,
            $insuredEventRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editedInsuredEvent = $insuredEvent->update(
                $insuredEventRequest->description,
                $insuredEventRequest->identifier,
                $insuredEventRequest->indemnity,
                $insuredEventRequest->eventDate,
                $insuredEventRequest->isClosed,
                $insuredEventRequest->contract,
                $insuredEventRequest->user
            );
            $this->insuredEventRepository->saveInsuredEvent($editedInsuredEvent);
            $this->addFlash(
                'success',
                'Pojistná událost je uložena.'
            );
        }
        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Pojistná událost',
            'goBackLink' => $this->generateUrl('adminApp.insuredEvent.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="adminApp.insuredEvent.delete")
     */
    public function deleteAction(string $id): Response
    {
        $insuredEvent = $this->insuredEventRepository->getInsuredEventById(
            Uuid::fromString($id)
        );

        $this->insuredEventRepository->deleteInsuredEvent($insuredEvent);

        $this->addFlash('success', 'Pojistná událost smazána.');
        return $this->redirectToRoute('adminApp.insuredEvent.list');
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\InsuredEvent;

use App\InsuredEvent\InsuredEvent;

class InsuredEventRequest
{

    /** @var string */
    public $description;

    /** @var string|null */
    public $identifier;

    /** @var int|null */
    public $indemnity;

    /** @var \DateTimeImmutable */
    public $eventDate;

    /** @var bool */
    public $isClosed;

    /** @var \App\Insurance\Contract|null */
    public $contract;

    /** @var \App\User\User */
    public $user;

    public static function fromInsuredEvent(InsuredEvent $insuredEvent): self
    {
        $request = new self();
        $request->description = $insuredEvent->getDescription();
        $request->identifier = $insuredEvent->getIdentifier();
        $request->indemnity = $insuredEvent->getIndemnity();
        $request->eventDate = $insuredEvent->getEventDate();
        $request->isClosed = $insuredEvent->isClosed();
        $request->contract = $insuredEvent->getContract();
        $request->user = $insuredEvent->getUser();
        return $request;
    }
}

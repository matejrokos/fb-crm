<?php declare(strict_types = 1);

namespace App\AdminApp\InsuredEvent;

use App\Insurance\Contract;
use App\User\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class InuredEventFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identifier', TextType::class, [
                'label' => 'Číslo pojistné události',
                'required' => false,
            ])
            ->add('indemnity', NumberType::class, [
                'label' => 'Pojistné plnění (Kč)',
                'required' => false,
            ])
            ->add('eventDate', DateType::class, [
                'label' => 'Datum poj. události',
                'required' => true,
                'empty_data' => '',
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
            ])
            ->add('isClosed', CheckboxType::class, [
                'label' => 'Uzavřeno',
                'required' => false,
            ])
            ->add('contract', EntityType::class, [
                'label' => 'Smlouva č.',
                'required' => true,
                'class' => Contract::class,
                'choice_label' => 'contractNumber',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Popis',
                'required' => true,
            ])
            ->add('user', EntityType::class, [
                'label' => 'Správce pojistné události',
                'required' => true,
                'class' => User::class,
                'choice_label' => 'name',
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit']);
    }
}

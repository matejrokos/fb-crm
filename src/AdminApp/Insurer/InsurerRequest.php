<?php declare(strict_types = 1);

namespace App\AdminApp\Insurer;

use App\Insurer\Insurer;
use Symfony\Component\Validator\Constraints as Assert;

class InsurerRequest
{

    /**
     * @Assert\NotBlank()
     * @var string
     */
    public $name;

    public static function createFromInsurer(Insurer $insurer): self
    {
        $insurerRequest = new self();
        $insurerRequest->name = $insurer->getName();
        return $insurerRequest;
    }
}

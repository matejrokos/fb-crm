<?php declare(strict_types = 1);

namespace App\AdminApp\Insurer;

use App\Insurer\InsurerFactory;
use App\Insurer\InsurerRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/insurer")
 */
class InsurerController extends AbstractController
{

    /** @var \App\Insurer\InsurerRepository */
    private $insurerRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    public function __construct(
        InsurerRepository $insurerRepository,
        FormFactoryInterface $formFactory
    )
    {
        $this->insurerRepository = $insurerRepository;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.insurer.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/Insurer/list.html.twig', [
            'insurers' => $this->insurerRepository->findAllInsurers(),
        ]);
    }

    /**
     * @Route("/new/", name="adminApp.insurer.create")
     */
    public function createAction(
        Request $request,
        InsurerFactory $insurerFactory
    ): Response
    {
        $insurerRequest = new InsurerRequest();
        $form = $this->formFactory->create(
            InsurerFormType::class,
            $insurerRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $insurer = $insurerFactory->createInsurer($insurerRequest->name);
            $this->insurerRepository->saveInsurer($insurer);
            $this->addFlash(
                'success',
                sprintf('Vytvořen pojistitel %s.', $insurer->getName())
            );

            return $this->redirectToRoute('adminApp.insurer.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nový pojistitel',
            'goBackLink' => $this->generateUrl('adminApp.insurer.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="adminApp.insurer.edit")
     */
    public function editAction(Request $request, string $id): Response
    {
        $insurer = $this->insurerRepository->getInsurerById(
            Uuid::fromString($id)
        );

        $insurerRequest = InsurerRequest::createFromInsurer($insurer);
        $form = $this->formFactory->create(
            InsurerFormType::class,
            $insurerRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $insurer->setName($insurerRequest->name);
            $this->insurerRepository->saveInsurer($insurer);

            $this->addFlash(
                'success',
                'Pojistitel uložen.'
            );

            return $this->redirectToRoute('adminApp.insurer.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Upravit pojistitele',
            'goBackLink' => $this->generateUrl('adminApp.insurer.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="adminApp.insurer.delete")
     */
    public function deleteAction(string $id): Response
    {
        $insurer = $this->insurerRepository->getInsurerById(
            Uuid::fromString($id)
        );

        $this->insurerRepository->deleteInsurer($insurer);

        $this->addFlash(
            'success',
            sprintf('Pojistitel %s byl smazán.', $insurer->getName())
        );
        return $this->redirectToRoute('adminApp.insurer.list');
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Insurance\InsuranceType;
use Symfony\Component\Validator\Constraints as Assert;

class InsuranceTypeRequest
{

    /**
     * @Assert\NotBlank()
     * @var string|null
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @var float
     */
    public $comission;

    public static function createFromInsuranceType(InsuranceType $insuranceType): self
    {
        $request = new self();
        $request->name = $insuranceType->getName();
        $request->comission = $insuranceType->getComission();
        return $request;
    }
}

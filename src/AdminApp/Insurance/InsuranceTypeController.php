<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Insurance\InsuranceTypeFactory;
use App\Insurance\InsuranceTypeRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/insurance-type")
 */
class InsuranceTypeController extends AbstractController
{

    /** @var \App\Insurance\InsuranceTypeRepository */
    private $insuranceTypeRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    public function __construct(
        InsuranceTypeRepository $insuranceTypeRepository,
        FormFactoryInterface $formFactory
    )
    {
        $this->insuranceTypeRepository = $insuranceTypeRepository;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.insuranceType.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/Insurance/insurance-type-list.html.twig', [
            'insuranceTypes' => $this->insuranceTypeRepository->findAllInsuranceTypes(),
        ]);
    }

    /**
     * @Route("/new/", name="adminApp.insuranceType.create")
     */
    public function createAction(
        Request $request,
        InsuranceTypeFactory $insuranceTypeFactory
    ): Response
    {
        $insuranceTypeRequest = new InsuranceTypeRequest();
        $form = $this->formFactory->create(
            InsuranceTypeFormType::class,
            $insuranceTypeRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $insuranceType = $insuranceTypeFactory->createInsuranceType(
                $insuranceTypeRequest->name,
                $insuranceTypeRequest->comission
            );
            $this->insuranceTypeRepository->saveInsuranceType($insuranceType);
            $this->addFlash(
                'success',
                sprintf('Vytvořen nový typ smlouvy %s.', $insuranceType->getName())
            );

            return $this->redirectToRoute('adminApp.insuranceType.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nový typ smlouvy',
            'goBackLink' => $this->generateUrl('adminApp.insuranceType.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="adminApp.insuranceType.edit")
     */
    public function editAction(Request $request, string $id): Response
    {
        $insuranceType = $this->insuranceTypeRepository->getInsuranceTypeById(
            Uuid::fromString($id)
        );

        $insuranceTypeRequest = InsuranceTypeRequest::createFromInsuranceType($insuranceType);
        $form = $this->formFactory->create(
            InsuranceTypeFormType::class,
            $insuranceTypeRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $insuranceType->setName($insuranceTypeRequest->name);
            $insuranceType->setComission($insuranceTypeRequest->comission);
            $this->insuranceTypeRepository->saveInsuranceType($insuranceType);

            $this->addFlash(
                'success',
                'Typ pojistné smlouvy uložen.'
            );

            return $this->redirectToRoute('adminApp.insuranceType.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Upravit typ smlouvy',
            'goBackLink' => $this->generateUrl('adminApp.insuranceType.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="adminApp.insuranceType.delete")
     */
    public function deleteAction(string $id): Response
    {
        $insuranceType = $this->insuranceTypeRepository->getInsuranceTypeById(
            Uuid::fromString($id)
        );

        $this->insuranceTypeRepository->deleteInsuranceType($insuranceType);

        $this->addFlash(
            'success',
            sprintf('Typ smlouvy %s byl smazán.', $insuranceType->getName())
        );
        return $this->redirectToRoute('adminApp.insuranceType.list');
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Insurance\Contract;
use App\Insurance\ContractFactory;

class ContractControllerFacade
{

    /** @var \App\Insurance\ContractFactory */
    public $contractFactory;

    public function __construct(
        ContractFactory $contractFactory
    )
    {
        $this->contractFactory = $contractFactory;
    }

    public function createContract(ContractRequest $contractRequest): Contract
    {
        return $this->contractFactory->createContract(
            $contractRequest->customer,
            $contractRequest->start,
            $contractRequest->end,
            $contractRequest->isRecurring,
            $contractRequest->contractNumber,
            (int) $contractRequest->price,
            (int) $contractRequest->comission,
            $contractRequest->note,
            $contractRequest->paymentFrequency,
            $contractRequest->insurer,
            $contractRequest->user,
            $contractRequest->insuranceType
        );
    }

    public function editContract(Contract $contract, ContractRequest $contractRequest): Contract
    {
        return $contract;
    }
}

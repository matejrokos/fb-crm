<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class InsuranceTypeFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název',
                'required' => true,
            ])
            ->add('comission', NumberType::class, [
                'label' => 'Provize',
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit']);
    }
}

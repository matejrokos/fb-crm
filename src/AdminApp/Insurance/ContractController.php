<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Insurance\ContractRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contract")
 */
class ContractController extends AbstractController
{

    /** @var \App\Insurance\ContractRepository */
    private $contractRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    public function __construct(
        ContractRepository $contractRepository,
        FormFactoryInterface $formFactory
    )
    {
        $this->contractRepository = $contractRepository;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.insuranceContract.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/Insurance/contract-list.html.twig', [
            'contracts' => $this->contractRepository->findAllContracts(),
        ]);
    }

    /**
     * @Route("/{id}/detail/", methods={"GET"}, name="adminApp.insuranceContract.detail")
     */
    public function detailAction(string $id): Response
    {
        $contract = $this->contractRepository->getContractById(
            Uuid::fromString($id)
        );

        return $this->render('@AdminApp/Insurance/contract-detail.html.twig', [
            'contract' => $contract,
            'goBackLink' => $this->generateUrl('adminApp.insuranceContract.list'),
        ]);
    }

    /**
     * @Route("/new/", name="adminApp.insuranceContract.create")
     */
    public function createAction(
        Request $request,
        ContractControllerFacade $controllerFacade
    ): Response
    {
        $contractRequest = new ContractRequest();
        $form = $this->formFactory->create(
            ContractFormType::class,
            $contractRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contract = $controllerFacade->createContract($contractRequest);
            $this->contractRepository->saveContract($contract);
            $this->addFlash(
                'success',
                'Smlouva v pořádku uložena.'
            );

            return $this->redirectToRoute('adminApp.insuranceContract.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nová smlouva',
            'goBackLink' => $this->generateUrl('adminApp.insuranceContract.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="adminApp.insuranceContract.edit")
     */
    public function editAction(
        Request $request,
        string $id,
        ContractControllerFacade $controllerFacade
    ): Response
    {
        $contract = $this->contractRepository->getContractById(
            Uuid::fromString($id)
        );

        $contractRequest = ContractRequest::createFromContract($contract);
        $form = $this->formFactory->create(
            ContractFormType::class,
            $contractRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editedContract = $controllerFacade->editContract($contract, $contractRequest);
            $this->contractRepository->saveContract($editedContract);

            $this->addFlash(
                'success',
                'Pojistná smlouva byla upravena'
            );

            return $this->redirectToRoute('adminApp.insuranceContract.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Upravit smlouvu',
            'goBackLink' => $this->generateUrl('adminApp.insuranceContract.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="adminApp.insuranceContract.delete")
     */
    public function deleteAction(string $id): Response
    {
        $contract = $this->contractRepository->getContractById(
            Uuid::fromString($id)
        );

        $this->contractRepository->deleteContract($contract);

        $this->addFlash('success', 'Smlouva smazána.');
        return $this->redirectToRoute('adminApp.insuranceContract.list');
    }
}

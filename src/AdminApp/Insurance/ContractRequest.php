<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Insurance\Contract;
use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

class ContractRequest
{

    /** @var \App\Customer\Customer */
    public $customer;

    /**
     * @Assert\Date()
     * @var \DateTimeImmutable
     */
    public $start;

    /**
     * @Assert\Date()
     * @var \DateTimeImmutable|null
     */
    public $end;

    /** @var bool */
    public $isRecurring;

    /** @var string */
    public $contractNumber;

    /** @var float */
    public $price;

    /** @var float */
    public $comission;

    /** @var string|null */
    public $note;

    /** @var \App\Insurance\PaymentFrequency */
    public $paymentFrequency;

    /** @var \App\Insurer\Insurer */
    public $insurer;

    /** @var \App\User\User */
    public $user;

    /** @var \App\Insurance\InsuranceType */
    public $insuranceType;

    public static function createFromContract(Contract $contract): self
    {
        $request = new self();
        $request->customer = $contract->getCustomer();
        $request->start = $contract->getStart();
        $request->end = $contract->hasEnd() ? $contract->getEnd() : null;
        $request->isRecurring = $contract->isRecurring();
        $request->contractNumber = $contract->getContractNumber();
        $request->price = $contract->getPrice();
        $request->comission = $contract->getComission();
        $request->note = $contract->getNote();
        $request->paymentFrequency = $contract->getPaymentFrequency();
        $request->insurer = $contract->getInsurer();
        $request->user = $contract->getUser();
        $request->insuranceType = $contract->getInsuranceType();
        return $request;
    }

    public function setEnd(?DateTimeImmutable $end): void
    {
        if ($end === null) {
            $this->end = null;
        } else {
            $this->end = $end;
        }
    }
}

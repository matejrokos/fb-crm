<?php declare(strict_types = 1);

namespace App\AdminApp\Insurance;

use App\Customer\Customer;
use App\Insurance\InsuranceType;
use App\Insurance\PaymentFrequency;
use App\Insurer\Insurer;
use App\User\User;
use Mhujer\ConsistenceBundle\FormType\EnumType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContractFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('customer', EntityType::class, [
                'label' => 'Klient',
                'required' => true,
                'class' => Customer::class,
                'choice_label' => 'name',
            ])
            ->add('contractNumber', TextType::class, [
                'label' => 'Číslo smlouvy',
                'required' => true,
            ])
            ->add('insuranceType', EntityType::class, [
                'label' => 'Typ',
                'required' => true,
                'class' => InsuranceType::class,
                'choice_label' => 'name',
            ])
            ->add('start', DateType::class, [
                'label' => 'Datum počátku',
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
                'required' => true,
            ])
            ->add('end', DateType::class, [
                'label' => 'Datum konce',
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
                'required' => false,
                'empty_data' => '',
            ])
            ->add('isRecurring', CheckboxType::class, [
                'label' => 'Automatická prolongace',
                'required' => false,
            ])
            ->add('price', NumberType::class, [
                'label' => 'Price',
                'required' => true,
            ])
            ->add('comission', NumberType::class, [
                'label' => 'Provize',
                'required' => true,
            ])
            ->add('paymentFrequency', EnumType::class, [
                'label' => 'Frekvence platby',
                'required' => true,
                'enum_class' => PaymentFrequency::class,
            ])
            ->add('insurer', EntityType::class, [
                'label' => 'Pojistitel',
                'required' => true,
                'class' => Insurer::class,
                'choice_label' => 'name',
            ])
            ->add('note', TextType::class, [
                'label' => 'Popis',
                'required' => false,
            ])
            ->add('user', EntityType::class, [
                'label' => 'Správce',
                'required' => true,
                'class' => User::class,
                'choice_label' => 'name',
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit']);
    }
}

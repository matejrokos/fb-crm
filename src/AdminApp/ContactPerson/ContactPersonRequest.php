<?php declare(strict_types = 1);

namespace App\AdminApp\ContactPerson;

use App\ContactPerson\ContactPerson;
use Symfony\Component\Validator\Constraints as Assert;

class ContactPersonRequest
{

    /** @var string|null */
    public $name;

    /**
     * @Assert\Email(
     *     message = "Je potřeba zadat platný e-mail."
     * )
     * @var string|null
     */
    public $email;

    /**
     * @Assert\Regex(
     *     pattern="/^[+]?[0-9 ]{9,}$/",
     *     message="Telefonní číslo nemá správný formát."
     * )
     * @var string|null
     */
    public $phone;

    public static function createFromContactPerson(ContactPerson $contactPerson): self
    {
        $contactPersonRequest = new self();
        $contactPersonRequest->name = $contactPerson->getName();
        $contactPersonRequest->email = $contactPerson->hasEmail() ? $contactPerson->getEmail() : null;
        $contactPersonRequest->phone = $contactPerson->hasPhone() ? $contactPerson->getPhone() : null;
        return $contactPersonRequest;
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\Customer;

use App\AdminApp\ContactPerson\ContactPersonRequest;
use App\Customer\Customer;
use Symfony\Component\Validator\Constraints as Assert;

class CustomerRequest
{

    /** @var string|null */
    public $identificationNumber;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    public $name;

    /** @var string|null */
    public $address;

    /** @var string|null */
    public $deliveryAddress;

    /** @var \App\AdminApp\ContactPerson\ContactPersonRequest */
    public $contactPersonPrimary;

    /** @var \App\AdminApp\ContactPerson\ContactPersonRequest */
    public $contactPersonSecondary;

    /** @var \App\Customer\CustomerType|null */
    public $customerType;

    /** @var string|null */
    public $note;

    /**
     * @Assert\NotBlank()
     * @var \App\User\User
     */
    public $user;

    public static function fromCustomer(Customer $customer): self
    {
        $customerRequest = new self();
        $customerRequest->identificationNumber = $customer->getIdentificationNumber();
        $customerRequest->name = $customer->getName();
        $customerRequest->address = $customer->getAddress();
        $customerRequest->deliveryAddress = $customer->getDeliveryAddress();
        $customerRequest->contactPersonPrimary = new ContactPersonRequest();
        $customerRequest->contactPersonSecondary = new ContactPersonRequest();
        $customerRequest->customerType = $customer->getCustomerType();
        $customerRequest->note = $customer->getNote();
        $customerRequest->user = $customer->getUser();

        return $customerRequest;
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\Customer;

use App\AdminApp\ContactPerson\ContactPersonFormType;
use App\Customer\CustomerType;
use App\User\User;
use Mhujer\ConsistenceBundle\FormType\EnumType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identificationNumber', TextType::class, [
                'label' => 'IČ/RČ',
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'label' => 'Jméno',
                'required' => true,
            ])
            ->add('address', TextareaType::class, [
                'label' => 'Trvalá adresa',
                'required' => true,
            ])
            ->add('deliveryAddress', TextareaType::class, [
                'label' => 'Korespondenční adresa',
                'required' => false,
            ])
            ->add('contactPersonPrimary', ContactPersonFormType::class, [
                'label' => 'Kontaktní osoba (na tu bude poslán mail',
                'required' => false,
            ])
            ->add('contactPersonSecondary', ContactPersonFormType::class, [
                'label' => 'Druhá kontaktní osoba',
                'required' => false,
            ])
            ->add('customerType', EnumType::class, [
                'enum_class' => CustomerType::class,
                'label' => 'Typ klienta',
                'required' => false,
            ])
            ->add('note', TextareaType::class, [
                'label' => 'Poznámka',
                'required' => false,
            ])
            ->add('user', EntityType::class, [
                'label' => 'Správce klienta',
                'required' => true,
                'class' => User::class,
                'choice_label' => 'name',
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CustomerRequest::class,
        ]);
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\Customer;

use App\Customer\CustomerRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/customer")
 */
class CustomerController extends AbstractController
{

    /** @var \App\Customer\CustomerRepository */
    private $customerRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    public function __construct(
        CustomerRepository $customerRepository,
        FormFactoryInterface $formFactory
    )
    {
        $this->customerRepository = $customerRepository;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.customer.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/Customer/list.html.twig', [
            'customers' => $this->customerRepository->findAllCustomers(),
        ]);
    }

    /**
     * @Route("/new/", name="adminApp.customer.create")
     */
    public function createAction(
        Request $request,
        CustomerControllerFacade $customerControllerFacade
    ): Response
    {
        $customerRequest = new CustomerRequest();
        $form = $this->formFactory->create(
            CustomerFormType::class,
            $customerRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $customerControllerFacade->createCustomer($customerRequest);
            $this->customerRepository->saveCustomer($customer);

            $this->addFlash(
                'success',
                'Klient uložen.'
            );

            return $this->redirectToRoute('adminApp.customer.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nový klient',
            'goBackLink' => $this->generateUrl('adminApp.customer.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/", name="adminApp.customer.detail")
     */
    public function detailAction(string $id): Response
    {
        $customer = $this->customerRepository->getCustomerById(
            Uuid::fromString($id)
        );

        return $this->render('@AdminApp/Customer/detail.html.twig', [
            'customer' => $customer,
            'goBackLink' => $this->generateUrl('adminApp.customer.list'),
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="adminApp.customer.edit")
     */
    public function editAction(
        Request $request,
        string $id,
        CustomerControllerFacade $customerControllerFacade
    ): Response
    {
        $customer = $this->customerRepository->getCustomerById(
            Uuid::fromString($id)
        );

        $customerRequest = CustomerRequest::fromCustomer($customer);
        $form = $this->formFactory->create(
            CustomerFormType::class,
            $customerRequest
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editedCustomer = $customerControllerFacade->editCustomer($customer, $customerRequest);
            $this->customerRepository->saveCustomer($editedCustomer);

            $this->addFlash(
                'success',
                sprintf('Klient %s je uložen.', $customer->getName())
            );

            return $this->redirectToRoute('adminApp.customer.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Upravit klienta',
            'goBackLink' => $this->generateUrl('adminApp.customer.list'),
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="adminApp.customer.delete")
     */
    public function deleteAction(string $id): Response
    {
        $customer = $this->customerRepository->getCustomerById(
            Uuid::fromString($id)
        );

        $this->customerRepository->deleteCustomer($customer);

        $this->addFlash(
            'success',
            sprintf('Klient %s byl smazán.', $customer->getName())
        );

        return $this->redirectToRoute('adminApp.customer.list');
    }
}

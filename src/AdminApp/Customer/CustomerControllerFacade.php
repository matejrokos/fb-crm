<?php declare(strict_types = 1);

namespace App\AdminApp\Customer;

use App\ContactPerson\ContactPersonRequestProcessor;
use App\Customer\Customer;
use App\Customer\CustomerFactory;

class CustomerControllerFacade
{

    /** @var \App\Customer\CustomerFactory */
    private $customerFactory;

    /** @var \App\ContactPerson\ContactPersonRequestProcessor */
    private $contactPersonRequestProcessor;

    public function __construct(
        CustomerFactory $customerFactory,
        ContactPersonRequestProcessor $contactPersonRequestProcessor
    )
    {
        $this->customerFactory = $customerFactory;
        $this->contactPersonRequestProcessor = $contactPersonRequestProcessor;
    }

    public function createCustomer(CustomerRequest $customerRequest): Customer
    {
        return $this->customerFactory->createCustomer(
            $customerRequest->identificationNumber,
            $customerRequest->name,
            $customerRequest->address,
            $customerRequest->deliveryAddress,
            $this->contactPersonRequestProcessor->createFromContactPersonRequest(
                $customerRequest->contactPersonPrimary
            ),
            $this->contactPersonRequestProcessor->createFromContactPersonRequest(
                $customerRequest->contactPersonSecondary
            ),
            $customerRequest->customerType,
            $customerRequest->note,
            $customerRequest->user
        );
    }

    public function editCustomer(
        Customer $customer,
        CustomerRequest $customerRequest
    ): Customer
    {
        $contactPersonPrimary = $this->contactPersonRequestProcessor->editContactPerson(
            $customer->getContactPersonPrimary(),
            $customerRequest->contactPersonPrimary
        );

        $conactPersonSecondary = $this->contactPersonRequestProcessor->editContactPerson(
            $customer->getContactPersonSecondary(),
            $customerRequest->contactPersonSecondary
        );

        $customer->setName($customerRequest->name);
        $customer->setIdentificationNumber($customerRequest->identificationNumber);
        $customer->setAddress($customerRequest->address);
        $customer->setDeliveryAddress($customerRequest->deliveryAddress);
        $customer->setContactPersonPrimary($contactPersonPrimary);
        $customer->setContactPersonSecondary($conactPersonSecondary);
        $customer->setCustomerType($customerRequest->customerType);
        $customer->setNote($customerRequest->note);
        $customer->setUser($customerRequest->user);
        return $customer;
    }
}

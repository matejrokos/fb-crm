<?php declare(strict_types = 1);

namespace App\AdminApp\User;

use App\User\UserRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /** @var \App\User\UserRepository  */
    private $userRepository;

    /** @var \Symfony\Component\Form\FormFactoryInterface */
    private $formFactory;

    /** @var \App\AdminApp\User\UserControllerFacade */
    private $userControllerFacade;

    public function __construct(
        UserRepository $userRepository,
        FormFactoryInterface $formFactory,
        UserControllerFacade $userControllerFacade
    )
    {
        $this->userRepository = $userRepository;
        $this->formFactory = $formFactory;
        $this->userControllerFacade = $userControllerFacade;
    }

    /**
     * @Route("/", methods={"GET"}, name="adminApp.user.list")
     */
    public function listAction(): Response
    {
        return $this->render('@AdminApp/User/list.html.twig', [
            'users' => $this->userRepository->findAllUsers(),
        ]);
    }

    /**
     * @Route("/new/", name="adminApp.user.create")
     */
    public function createAction(Request $request): Response
    {
        $userRequest = new UserRequest();
        $userForm = $this->formFactory->create(
            UserFormType::class,
            $userRequest
        );
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            try {
                $user = $this->userControllerFacade->createUser($userRequest);
                $this->userRepository->saveUser($user);
                $this->addFlash(
                    'success',
                    sprintf('Vytvořen uživatel %s.', $user->getName())
                );
            } catch (\App\AdminApp\User\Exception\CannotCreateUserWithoutPasswordException $e) {
                $this->addFlash(
                    'warning',
                    'Nelze vytvořit uživatele bez hesla.'
                );
            }

            return $this->redirectToRoute('adminApp.user.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Nový uživatel',
            'goBackLink' => $this->generateUrl('adminApp.user.list'),
            'userForm' => $userForm->createView(),
        ]);
    }

    /**
     * @Route("/{userId}/edit/", name="adminApp.user.edit")
     */
    public function editAction(Request $request, string $userId): Response
    {
        $user = $this->userRepository->getUserById(
            Uuid::fromString($userId)
        );

        $userRequest = UserRequest::createFromUser($user);
        $userForm = $this->formFactory->create(
            UserFormType::class,
            $userRequest
        );
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $editedUser = $this->userControllerFacade->editUser($user, $userRequest);
            $this->userRepository->saveUser($editedUser);

            $this->addFlash(
                'success',
                sprintf('Uživatel %s byl upraven.', $user->getName())
            );

            return $this->redirectToRoute('adminApp.user.list');
        }

        return $this->render('@AdminApp/add-edit.html.twig', [
            'title' => 'Upravit uživatele',
            'goBackLink' => $this->generateUrl('adminApp.user.list'),
            'userForm' => $userForm->createView(),
        ]);
    }

    /**
     * @Route("/{userId}/delete", methods={"POST"}, name="adminApp.user.delete")
     */
    public function deleteAction(string $userId): Response
    {
        $user = $this->userRepository->getUserById(
            Uuid::fromString($userId)
        );

        $this->userRepository->deleteUser($user);

        $this->addFlash(
            'success',
            sprintf('Uživatel %s byl smazán.', $user->getName())
        );
        return $this->redirectToRoute('adminApp.user.list');
    }
}

<?php declare(strict_types = 1);

namespace App\AdminApp\User;

use App\User\User;
use Symfony\Component\Validator\Constraints as Assert;

class UserRequest
{

    /**
     * @Assert\NotBlank()
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "Je potřeba zadat platný e-mail."
     * )
     * @var string
     */
    public $email;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    public $userName;

    /**
     * @Assert\Length(
     *     min=6,
     *     minMessage="Heslo musí být alespoň {{ limit }} znaků dlouhé"
     * )
     * @var string|null
     */
    public $password;

    /**
     * @Assert\NotBlank()
     * @var \App\User\UserRole
     */
    public $userRole;

    public static function createFromUser(User $user): self
    {
        $userRequest = new self();
        $userRequest->name = $user->getName();
        $userRequest->email = $user->getEmail();
        $userRequest->userName = $user->getUserName();
        $userRequest->userRole = $user->getUserRole();

        return $userRequest;
    }
}

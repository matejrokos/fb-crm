<?php declare(strict_types = 1);

namespace App\AdminApp\User;

use App\User\UserRole;
use Mhujer\ConsistenceBundle\FormType\EnumType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Jméno',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'required' => true,
            ])
            ->add('userName', TextType::class, [
                'label' => 'Uživatelské jméno',
                'required' => true,
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Heslo',
                'required' => true,
            ])
            ->add('userRole', EnumType::class, [
                'label' => 'Role',
                'required' => true,
                'enum_class' => UserRole::class,
            ])
            ->add('save', SubmitType::class, ['label' => 'Uložit']);
    }
}

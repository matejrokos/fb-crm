<?php declare(strict_types = 1);

namespace App\AdminApp\User\Exception;

class CannotCreateUserWithoutPasswordException extends \Exception
{

    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct('Cannot create user without password.', 0, $previous);
    }
}

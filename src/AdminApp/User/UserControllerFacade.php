<?php declare(strict_types = 1);

namespace App\AdminApp\User;

use App\User\User;
use App\User\UserFactory;

class UserControllerFacade
{

    /** @var \App\User\UserFactory  */
    private $userFactory;

    public function __construct(
        UserFactory $userFactory
    )
    {
        $this->userFactory = $userFactory;
    }

    public function editUser(User $user, UserRequest $request): User
    {
        $user->setEmail($request->email);
        $user->setName($request->name);
        if ($request->password !== null) {
            $user->setPassword($request->password);
        }
        $user->setUserName($request->userName);
        $user->setUserRole($request->userRole);
        return $user;
    }

    public function createUser(UserRequest $request): User
    {
        if ($request->password === null) {
            throw new \App\AdminApp\User\Exception\CannotCreateUserWithoutPasswordException();
        }

        return $this->userFactory->createUser(
            $request->name,
            $request->email,
            $request->userName,
            $request->password,
            $request->userRole
        );
    }
}

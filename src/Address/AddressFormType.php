<?php declare(strict_types = 1);

namespace App\Address;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressFormType extends AbstractType
{

    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Název / Jméno',
                'required' => false,
            ])
            ->add('street', TextType::class, [
                'label' => 'Ulice',
                'required' => true,
            ])
            ->add('number', TextType::class, [
                'label' => 'Č.P./Č.O.',
                'required' => true,
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'PSČ',
                'required' => true,
            ])
            ->add('city', TextType::class, [
                'label' => 'Město',
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AddressRequest::class,
        ]);
    }
}

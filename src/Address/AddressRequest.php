<?php declare(strict_types = 1);

namespace App\Address;

class AddressRequest
{

    /** @var string */
    public $name;

    /** @var string */
    public $street;

    /** @var string */
    public $number;

    /** @var string */
    public $zipCode;

    /** @var string */
    public $city;

    public static function fromAddressEmbeddable(AddressEmbeddable $addressEmbeddable): self
    {
        $addressRequest = new self();
        $addressRequest->name = $addressEmbeddable->getName();
        $addressRequest->street = $addressEmbeddable->getStreet();
        $addressRequest->number = $addressEmbeddable->getNumber();
        $addressRequest->zipCode = $addressEmbeddable->getZipCode();
        $addressRequest->city = $addressEmbeddable->getCity();

        return $addressRequest;
    }
}

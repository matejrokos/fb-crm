<?php declare(strict_types = 1);

namespace App\Address;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class AddressEmbeddable
{

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $street;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $city;

    public function __construct(
        string $name,
        string $street,
        string $number,
        string $zipCode,
        string $city
    )
    {
        $this->name = $name;
        $this->street = $street;
        $this->number = $number;
        $this->zipCode = $zipCode;
        $this->city = $city;
    }

    public static function fromAddressRequest(AddressRequest $addressRequest): self
    {
        return new self(
            $addressRequest->name,
            $addressRequest->street,
            $addressRequest->number,
            $addressRequest->zipCode,
            $addressRequest->city
        );
    }

    public function isEmpty(): bool
    {
        if ($this->name !== null) {
            return false;
        }
        if ($this->street !== null) {
            return false;
        }
        if ($this->number !== null) {
            return false;
        }
        if ($this->zipCode !== null) {
            return false;
        }
        if ($this->city !== null) {
            return false;
        }

        return true;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function hasStreet(): bool
    {
        return $this->street !== null;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function hasNumber(): bool
    {
        return $this->number !== null;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function hasZipCode(): bool
    {
        return $this->zipCode !== null;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function hasCity(): bool
    {
        return $this->city !== null;
    }

    public function getCity(): string
    {
        return $this->city;
    }
}

<?php declare(strict_types = 1);

namespace App\Insurer;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class InsurerFactory
{

    public function createInsurer(string $name): Insurer
    {
        return new Insurer(
            Uuid::uuid4(),
            $name,
            new DateTimeImmutable()
        );
    }
}

<?php declare(strict_types = 1);

namespace App\Insurer;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class InsurerRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getInsurerById(UuidInterface $id): Insurer
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select([
                'insurer',
            ])
            ->from(Insurer::class, 'insurer')
            ->andWhere('insurer.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllInsurers(): InsurerList
    {
        $insurers = $this->entityManager
            ->createQueryBuilder()
            ->select([
                'insurer',
            ])
            ->from(Insurer::class, 'insurer')
            ->orderBy('insurer.name')
            ->getQuery()
            ->getResult();

        return new InsurerList($insurers);
    }

    public function saveInsurer(Insurer $insurer): void
    {
        $this->entityManager->persist($insurer);
        $this->entityManager->flush();
    }

    public function deleteInsurer(Insurer $insurer): void
    {
        $this->entityManager->remove($insurer);
        $this->entityManager->flush();
    }
}

<?php declare(strict_types = 1);

namespace App\Insurer;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class Insurer
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        string $name,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }
}

<?php declare(strict_types = 1);

namespace App\Insurer;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class InsurerList extends EntityList
{

    /** @param \App\Insurer\Insurer[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, Insurer::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\Insurer\Insurer[]
     */
    public function getItems(): array
    {
        /** @var \App\Insurer\Insurer[] $items */
        $items = parent::getItems();
        return $items;
    }
}

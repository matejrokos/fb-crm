<?php declare(strict_types = 1);

namespace App\User;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class UserRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getUserById(UuidInterface $id): User
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select([
                'user',
            ])
            ->from(User::class, 'user')
            ->andWhere('user.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllUsers(): UserList
    {
        $users = $this->entityManager
            ->createQueryBuilder()
            ->select([
                'user',
            ])
            ->from(User::class, 'user')
            ->orderBy('user.userName')
            ->getQuery()
            ->getResult();

        return new UserList($users);
    }

    public function saveUser(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function deleteUser(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
}

<?php declare(strict_types = 1);

namespace App\User;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class UserFactory
{

    public function createUser(
        string $name,
        string $email,
        string $userName,
        string $password,
        UserRole $userRole
    ): User
    {
        return new User(
            Uuid::uuid4(),
            $name,
            $email,
            $userName,
            $password,
            $userRole,
            new DateTimeImmutable()
        );
    }
}

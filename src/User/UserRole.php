<?php declare(strict_types = 1);

namespace App\User;

use Consistence\Enum\Enum;

class UserRole extends Enum
{

    public const ADMIN = 'admin';
    public const EMPLOYEE = 'employee';
}

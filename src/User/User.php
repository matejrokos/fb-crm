<?php declare(strict_types = 1);

namespace App\User;

use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class User
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, options={"collation": "ascii_general_ci"}, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     * @var string
     */
    private $userName;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $password;

    /**
     * @Enum(class=UserRole::class)
     * @ORM\Column(type="string_enum")
     * @var \App\User\UserRole
     */
    private $userRole;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        string $name,
        string $email,
        string $userName,
        string $password,
        UserRole $userRole,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->userName = $userName;
        $this->password = $password;
        $this->userRole = $userRole;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getUserRole(): UserRole
    {
        return $this->userRole;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    public function setUserRole(UserRole $userRole): void
    {
        $this->userRole = $userRole;
    }
}

<?php declare(strict_types = 1);

namespace App\User;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class UserList extends EntityList
{

    /** @param \App\User\User[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, User::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\User\User[]
     */
    public function getItems(): array
    {
        /** @var \App\User\User[] $items */
        $items = parent::getItems();
        return $items;
    }
}

<?php declare(strict_types = 1);

namespace App\CalendarEvent;

use Consistence\Enum\Enum;

class CalendarEventColour extends Enum
{

    public const RED = 'red';
    public const BLUE = 'blue';
    public const GREEN = 'green';
    public const VIOLET = 'violet';
}

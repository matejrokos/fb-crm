<?php declare(strict_types = 1);

namespace App\CalendarEvent;

use App\User\User;
use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class CalendarEvent
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="\App\User\User")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\User\User
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $dateTimeFrom;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $dateTimeTo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $notificationMinutesBefore;

    /**
     * @Enum(class=CalendarEventColour::class)
     * @ORM\Column(type="string_enum")
     * @var \App\CalendarEvent\CalendarEventColour
     */
    private $colour;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        string $description,
        User $user,
        DateTimeImmutable $dateTimeFrom,
        DateTimeImmutable $dateTimeTo,
        ?int $notificationMinutesBefore,
        CalendarEventColour $colour,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->description = $description;
        $this->user = $user;
        $this->dateTimeFrom = $dateTimeFrom;
        $this->dateTimeTo = $dateTimeTo;
        $this->notificationMinutesBefore = $notificationMinutesBefore;
        $this->colour = $colour;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getDateTimeFrom(): DateTimeImmutable
    {
        return $this->dateTimeFrom;
    }

    public function getDateTimeTo(): DateTimeImmutable
    {
        return $this->dateTimeTo;
    }

    public function hasNotificationMinutesBefore(): bool
    {
        return $this->notificationMinutesBefore !== null;
    }

    public function getNotificationMinutesBefore(): ?int
    {
        return $this->notificationMinutesBefore;
    }

    public function getColour(): CalendarEventColour
    {
        return $this->colour;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }
}

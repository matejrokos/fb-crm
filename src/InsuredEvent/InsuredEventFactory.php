<?php declare(strict_types = 1);

namespace App\InsuredEvent;

use App\AdminApp\InsuredEvent\InsuredEventRequest;
use App\Insurance\Contract;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class InsuredEventFactory
{

    public function createInsuredEvent(
        InsuredEventRequest $request,
        Contract $contract
    ): InsuredEvent
    {
        return new InsuredEvent(
            Uuid::uuid4(),
            $request->description,
            $request->identifier,
            (int) $request->indemnity,
            $request->eventDate,
            $request->isClosed,
            $contract,
            $request->user,
            new DateTimeImmutable()
        );
    }
}

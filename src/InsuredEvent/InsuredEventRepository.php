<?php declare(strict_types = 1);

namespace App\InsuredEvent;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class InsuredEventRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getInsuredEventById(UuidInterface $id): InsuredEvent
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select('InsuredEvent')
            ->from(InsuredEvent::class, 'InsuredEvent')
            ->andWhere('InsuredEvent.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllInsuredEvents(): InsuredEventList
    {
        $items = $this->entityManager
            ->createQueryBuilder()
            ->select('InsuredEvent')
            ->from(InsuredEvent::class, 'InsuredEvent')
            ->orderBy('InsuredEvent.eventDate', 'DESC')
            ->getQuery()
            ->getResult();

        return new InsuredEventList($items);
    }

    public function saveInsuredEvent(InsuredEvent $insuredEvent): void
    {
        $this->entityManager->persist($insuredEvent);
        $this->entityManager->flush();
    }

    public function deleteInsuredEvent(InsuredEvent $insuredEvent): void
    {
        $this->entityManager->remove($insuredEvent);
        $this->entityManager->flush();
    }
}

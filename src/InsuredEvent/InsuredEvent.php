<?php declare(strict_types = 1);

namespace App\InsuredEvent;

use App\Insurance\Contract;
use App\User\User;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class InsuredEvent
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true, length=50)
     * @var string|null
     */
    private $identifier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $indemnity;

    /**
     * @ORM\Column(type="date_immutable")
     * @var \DateTimeImmutable
     */
    private $eventDate;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $isClosed;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Insurance\Contract")
     * @ORM\JoinColumn(nullable=true)
     * @var \App\Insurance\Contract|null
     */
    private $contract;

    /**
     * @ORM\ManyToOne(targetEntity="\App\User\User")
     * @var \App\User\User
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        string $description,
        string $identifier,
        int $indemnity,
        DateTimeImmutable $eventDate,
        bool $isClosed,
        ?Contract $contract,
        User $user,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->description = $description;
        $this->identifier = $identifier;
        $this->indemnity = $indemnity;
        $this->eventDate = $eventDate;
        $this->isClosed = $isClosed;
        $this->contract = $contract;
        $this->user = $user;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getIndemnity(): ?int
    {
        return $this->indemnity;
    }

    public function getEventDate(): DateTimeImmutable
    {
        return $this->eventDate;
    }

    public function isClosed(): bool
    {
        return $this->isClosed;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }

    public function update(
        string $description,
        string $identifier,
        int $indemnity,
        DateTimeImmutable $eventDate,
        bool $isClosed,
        ?Contract $contract,
        User $user
    ): self
    {
        $this->description = $description;
        $this->identifier = $identifier;
        $this->indemnity = $indemnity;
        $this->eventDate = $eventDate;
        $this->isClosed = $isClosed;
        $this->contract = $contract;
        $this->user = $user;

        return $this;
    }
}

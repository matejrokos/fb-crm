<?php declare(strict_types = 1);

namespace App\InsuredEvent;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class InsuredEventList extends EntityList
{

    /** @param \App\InsuredEvent\InsuredEvent[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, InsuredEvent::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\InsuredEvent\InsuredEvent[]
     */
    public function getItems(): array
    {
        /** @var \App\InsuredEvent\InsuredEvent[] $items */
        $items = parent::getItems();
        return $items;
    }
}

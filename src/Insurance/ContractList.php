<?php declare(strict_types = 1);

namespace App\Insurance;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class ContractList extends EntityList
{

    /** @param \App\Insurance\Contract[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, Contract::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\Insurance\Contract[]
     */
    public function getItems(): array
    {
        /** @var \App\Insurance\Contract[] $items */
        $items = parent::getItems();
        return $items;
    }
}

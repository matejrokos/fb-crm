<?php declare(strict_types = 1);

namespace App\Insurance;

use App\EntityList\EntityList;
use Consistence\Type\Type;

class InsuranceTypeList extends EntityList
{

    /** @param \App\Insurance\InsuranceType[] $items */
    public function __construct(array $items)
    {
        Type::checkType($items, InsuranceType::class . '[]');
        parent::__construct($items);
    }

    /**
     * @return \App\Insurance\InsuranceType[]
     */
    public function getItems(): array
    {
        /** @var \App\Insurance\InsuranceType[] $items */
        $items = parent::getItems();
        return $items;
    }
}

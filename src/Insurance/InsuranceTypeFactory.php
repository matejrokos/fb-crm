<?php declare(strict_types = 1);

namespace App\Insurance;

use Ramsey\Uuid\Uuid;

class InsuranceTypeFactory
{

    public function createInsuranceType(string $name, float $comission): InsuranceType
    {
        return new InsuranceType(
            Uuid::uuid4(),
            $name,
            $comission
        );
    }
}

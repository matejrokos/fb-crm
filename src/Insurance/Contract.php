<?php declare(strict_types = 1);

namespace App\Insurance;

use App\Customer\Customer;
use App\Insurer\Insurer;
use App\User\User;
use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class Contract
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Customer\Customer")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\Customer\Customer
     */
    private $customer;

    /**
     * @ORM\Column(type="date_immutable")
     * @var \DateTimeImmutable
     */
    private $start;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @var \DateTimeImmutable|null
     */
    private $end;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $isRecurring;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $contractNumber;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $comission;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $note;

    /**
     * @Enum(class=App\Insurance\PaymentFrequency::class)
     * @ORM\Column(type="string_enum")
     * @var \App\Insurance\PaymentFrequency
     */
    private $paymentFrequency;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Insurer\Insurer")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\Insurer\Insurer
     */
    private $insurer;

    /**
     * @ORM\ManyToOne(targetEntity="\App\User\User")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\User\User
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Insurance\InsuranceType")
     * @ORM\JoinColumn(nullable=false)
     * @var \App\Insurance\InsuranceType
     */
    private $insuranceType;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdOn;

    public function __construct(
        UuidInterface $id,
        Customer $customer,
        DateTimeImmutable $start,
        ?DateTimeImmutable $end,
        bool $isRecurring,
        string $contractNumber,
        int $price,
        int $comission,
        ?string $note,
        PaymentFrequency $paymentFrequency,
        Insurer $insurer,
        User $user,
        InsuranceType $insuranceType,
        DateTimeImmutable $createdOn
    )
    {
        $this->id = $id;
        $this->customer = $customer;
        $this->start = $start;
        $this->end = $end;
        $this->isRecurring = $isRecurring;
        $this->contractNumber = $contractNumber;
        $this->price = $price;
        $this->comission = $comission;
        $this->note = $note;
        $this->paymentFrequency = $paymentFrequency;
        $this->insurer = $insurer;
        $this->user = $user;
        $this->insuranceType = $insuranceType;
        $this->createdOn = $createdOn;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getStart(): DateTimeImmutable
    {
        return $this->start;
    }

    public function getEnd(): DateTimeImmutable
    {
        return $this->end;
    }

    public function hasEnd(): bool
    {
        return $this->end !== null;
    }

    public function isRecurring(): bool
    {
        return $this->isRecurring;
    }

    public function getContractNumber(): string
    {
        return $this->contractNumber;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getComission(): int
    {
        return $this->comission;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getPaymentFrequency(): PaymentFrequency
    {
        return $this->paymentFrequency;
    }

    public function getInsurer(): Insurer
    {
        return $this->insurer;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getInsuranceType(): InsuranceType
    {
        return $this->insuranceType;
    }

    public function getCreatedOn(): DateTimeImmutable
    {
        return $this->createdOn;
    }
}

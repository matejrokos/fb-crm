<?php declare(strict_types = 1);

namespace App\Insurance;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class ContractRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getContractById(UuidInterface $id): Contract
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select('contract')
            ->from(Contract::class, 'contract')
            ->andWhere('contract.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllContracts(): ContractList
    {
        $contracts = $this->entityManager
            ->createQueryBuilder()
            ->select('contract')
            ->from(Contract::class, 'contract')
            ->orderBy('contract.start', 'DESC')
            ->getQuery()
            ->getResult();

        return new ContractList($contracts);
    }

    public function saveContract(Contract $contract): void
    {
        $this->entityManager->persist($contract);
        $this->entityManager->flush();
    }

    public function deleteContract(Contract $contract): void
    {
        $this->entityManager->remove($contract);
        $this->entityManager->flush();
    }
}

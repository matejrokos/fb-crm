<?php declare(strict_types = 1);

namespace App\Insurance;

use App\Customer\Customer;
use App\Insurer\Insurer;
use App\User\User;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class ContractFactory
{

    public function createContract(
        Customer $customer,
        DateTimeImmutable $start,
        ?DateTimeImmutable $end,
        bool $isRecurring,
        string $contractNumber,
        int $price,
        int $comission,
        ?string $note,
        PaymentFrequency $paymentFrequency,
        Insurer $insurer,
        User $user,
        InsuranceType $insuranceType
    ): Contract
    {
        return new Contract(
            Uuid::uuid4(),
            $customer,
            $start,
            $end,
            $isRecurring,
            $contractNumber,
            $price,
            $comission,
            $note,
            $paymentFrequency,
            $insurer,
            $user,
            $insuranceType,
            new DateTimeImmutable()
        );
    }
}

<?php declare(strict_types = 1);

namespace App\Insurance;

use Consistence\Enum\Enum;

class PaymentFrequency extends Enum
{

    public const YEARLY = 'yearly';
    public const QUARTERLY = 'quarterly';
    public const HALF_YEARLY = 'half-yearly';
    public const MONTHLY = 'monthly';
}

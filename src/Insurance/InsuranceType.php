<?php declare(strict_types = 1);

namespace App\Insurance;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 */
class InsuranceType
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @var float|string
     */
    private $comission;

    public function __construct(
        UuidInterface $id,
        string $name,
        float $comission
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->comission = $comission;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getComission(): float
    {
        return (float) $this->comission;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setComission(float $comission): void
    {
        $this->comission = $comission;
    }
}

<?php declare(strict_types = 1);

namespace App\Insurance;

use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

class InsuranceTypeRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface; */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getInsuranceTypeById(UuidInterface $id): InsuranceType
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select('InsuranceType')
            ->from(InsuranceType::class, 'InsuranceType')
            ->andWhere('InsuranceType.id = :id')
            ->setParameter('id', $id->toString())
            ->getQuery()
            ->getSingleResult();
    }

    public function findAllInsuranceTypes(): InsuranceTypeList
    {
        $insuranceTypes = $this->entityManager
            ->createQueryBuilder()
            ->select('InsuranceType')
            ->from(InsuranceType::class, 'InsuranceType')
            ->orderBy('InsuranceType.name')
            ->getQuery()
            ->getResult();

        return new InsuranceTypeList($insuranceTypes);
    }

    public function saveInsuranceType(InsuranceType $insuranceType): void
    {
        $this->entityManager->persist($insuranceType);
        $this->entityManager->flush();
    }

    public function deleteInsuranceType(InsuranceType $insuranceType): void
    {
        $this->entityManager->remove($insuranceType);
        $this->entityManager->flush();
    }
}

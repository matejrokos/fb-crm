<?php declare(strict_types = 1);

namespace App\EntityList;

interface EntityListInterface
{

    /** @param object[] $items  */
    public function __construct(array $items);

    /** @return object[] */
    public function getItems(): array;

    public function getItemsCount(): int;

    public function hasItems(): bool;
}

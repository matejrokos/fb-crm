<?php declare(strict_types = 1);

namespace App\EntityList;

class EntityList implements EntityListInterface
{

    /** @var object[] */
    private $items;

    /** @param object[] $items  */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /** @return object[] */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getItemsCount(): int
    {
        return count($this->items);
    }

    public function hasItems(): bool
    {
        return $this->getItemsCount() > 0;
    }
}

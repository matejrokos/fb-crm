<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200116112341 extends AbstractMigration
{

    public function getDescription(): string
    {
        return 'Contract';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contract (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', customer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', insurer_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', insurance_type_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', start DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', end DATE DEFAULT NULL COMMENT \'(DC2Type:date_immutable)\', is_recurring TINYINT(1) NOT NULL, contract_number VARCHAR(255) NOT NULL, price INT NOT NULL, comission INT NOT NULL, note VARCHAR(255) DEFAULT NULL, payment_frequency VARCHAR(255) NOT NULL COMMENT \'(DC2Type:string_enum)\', created_on DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_E98F28599395C3F3 (customer_id), INDEX IDX_E98F2859895854C7 (insurer_id), INDEX IDX_E98F2859A76ED395 (user_id), INDEX IDX_E98F2859286DA936 (insurance_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F28599395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859895854C7 FOREIGN KEY (insurer_id) REFERENCES insurer (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859286DA936 FOREIGN KEY (insurance_type_id) REFERENCES insurance_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contract');
    }
}

<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200112072220 extends AbstractMigration
{

    public function getDescription(): string
    {
        return 'Create Customer, Insurer and user tables';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, identification_number VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, delivery_address LONGTEXT DEFAULT NULL, customer_type VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:string_enum)\', note LONGTEXT DEFAULT NULL, created_on DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', contact_person_primary_name VARCHAR(100) DEFAULT NULL, contact_person_primary_email VARCHAR(100) DEFAULT NULL COLLATE `ascii_general_ci`, contact_person_primary_phone VARCHAR(30) DEFAULT NULL, contact_person_secondary_name VARCHAR(100) DEFAULT NULL, contact_person_secondary_email VARCHAR(100) DEFAULT NULL COLLATE `ascii_general_ci`, contact_person_secondary_phone VARCHAR(30) DEFAULT NULL, UNIQUE INDEX UNIQ_81398E09347639A5 (identification_number), INDEX IDX_81398E09A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE insurer (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, created_on DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) NOT NULL, email VARCHAR(100) NOT NULL COLLATE `ascii_general_ci`, user_name VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, user_role VARCHAR(255) NOT NULL COMMENT \'(DC2Type:string_enum)\', created_on DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09A76ED395');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE insurer');
        $this->addSql('DROP TABLE user');
    }
}

###> symfony/framework-bundle ###
APP_ENV=cl
APP_SECRET=s0meS3cr3tStr1ng
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
DATABASE_URL=mysql://root:root@127.0.0.1:9101/crm?serverVersion=5.7
###< doctrine/doctrine-bundle ###

###> symfony/mailer ###
# MAILER_DSN=smtp://localhost
###< symfony/mailer ###

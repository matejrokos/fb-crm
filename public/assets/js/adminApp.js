"use strict";

Array.from(document.getElementsByClassName('action-delete')).forEach(function(item) {
    item.addEventListener('click', function (event) {
        event.preventDefault();
        if (confirm('Opravdu smazat?')) {
            window.location = this.href;
        }
    });
});
